<?php


namespace Esol\ProductSearchBundle\Service\Domain;


use Esol\ProductSearchBundle\Entity\Variation;

interface VariationDomainServiceInterface
{
    public static function getVariation(Variation $variation):Variation;
}