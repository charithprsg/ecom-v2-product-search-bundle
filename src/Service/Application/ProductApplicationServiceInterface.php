<?php

namespace Esol\ProductSearchBundle\Service\Application;

use Esol\ProductSearchBundle\Entity\Product;

interface ProductApplicationServiceInterface
{
    public function getProductByErpCode(string $erpCode):Product;
}