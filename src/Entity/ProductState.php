<?php

namespace Esol\ProductSearchBundle\Entity;
use Esol\ProductSearchBundle\Repository\ProductStateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=Esol\ProductSearchBundle\Repository\ProductStateRepository::class)
 * @ORM\Table(name="product_product_state")
 */
class ProductState
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * EDIT,PENDING_APPROVE,APPROVED,DELETED
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
