<?php

namespace Esol\ProductSearchBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=Esol\ProductSearchBundle\Repository\VariationRepository::class)
 */
class Variation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="variations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $erp;

    /**
     * @ORM\Column(type="float")
     */
    private $mrp;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mop;

    /**
     * @ORM\Column(type="float")
     */
    private $stock;

    /**
     * @ORM\ManyToMany(targetEntity=FilterItem::class)
     */
    private $filterItems;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->filterItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getErp(): ?string
    {
        return $this->erp;
    }

    public function setErp(string $erp): self
    {
        $this->erp = $erp;

        return $this;
    }

    public function getMrp(): ?float
    {
        return $this->mrp;
    }

    public function setMrp(float $mrp): self
    {
        $this->mrp = $mrp;

        return $this;
    }

    public function getMop(): ?float
    {
        return $this->mop;
    }

    public function setMop(?float $mop): self
    {
        $this->mop = $mop;

        return $this;
    }

    public function getStock(): ?float
    {
        return $this->stock;
    }

    public function setStock(float $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection|FilterItem[]
     */
    public function getFilterItems(): Collection
    {
        return $this->filterItems;
    }

    public function addFilterItem(FilterItem $filterItem): self
    {
        if (!$this->filterItems->contains($filterItem)) {
            $this->filterItems[] = $filterItem;
        }

        return $this;
    }

    public function removeFilterItem(FilterItem $filterItem): self
    {
        $this->filterItems->removeElement($filterItem);

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
