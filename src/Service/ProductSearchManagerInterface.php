<?php


namespace Esol\ProductSearchBundle\Service;



use Esol\ProductSearchBundle\DataModel\InputModel\ProductManager\ProductSearchParaModel;

interface ProductSearchManagerInterface
{
    public function getProductByErpCode(string $erpCode);
}