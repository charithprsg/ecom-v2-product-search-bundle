<?php

namespace Esol\ProductSearchBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=Esol\ProductSearchBundle\Repository\ProductRepository::class)
 * @ORM\Table(name="product_product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $parentCode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mop;

    /**
     * @ORM\Column(type="float")
     */
    private $mrp;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=ProductState::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity=DepartmentHierarchy::class, mappedBy="product")
     */
    private $departmentHierarchies;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $siteCode;

    /**
     * @ORM\ManyToMany(targetEntity=Filter::class)
     */
    private $filters;

    /**
     * @ORM\OneToMany(targetEntity=Variation::class, mappedBy="product")
     */
    private $variations;

    /**
     * @ORM\ManyToOne(targetEntity=Brand::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\Column(type="integer")
     */
    private $vendorId;

    /**
     * @ORM\OneToMany(targetEntity=Image::class, mappedBy="product")
     */
    private $images;

    public function __construct()
    {
        $this->departmentHierarchies = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->variations = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParentCode(): ?string
    {
        return $this->parentCode;
    }

    public function setParentCode(string $parentCode): self
    {
        $this->parentCode = $parentCode;

        return $this;
    }

    public function getMop(): ?float
    {
        return $this->mop;
    }

    public function setMop(?float $mop): self
    {
        $this->mop = $mop;

        return $this;
    }

    public function getMrp(): ?float
    {
        return $this->mrp;
    }

    public function setMrp(float $mrp): self
    {
        $this->mrp = $mrp;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getState(): ?ProductState
    {
        return $this->state;
    }

    public function setState(?ProductState $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|DepartmentHierarchy[]
     */
    public function getDepartmentHierarchies(): Collection
    {
        return $this->departmentHierarchies;
    }

    public function addDepartmentHierarchy(DepartmentHierarchy $departmentHierarchy): self
    {
        if (!$this->departmentHierarchies->contains($departmentHierarchy)) {
            $this->departmentHierarchies[] = $departmentHierarchy;
            $departmentHierarchy->setProduct($this);
        }

        return $this;
    }

    public function removeDepartmentHierarchy(DepartmentHierarchy $departmentHierarchy): self
    {
        if ($this->departmentHierarchies->removeElement($departmentHierarchy)) {
            // set the owning side to null (unless already changed)
            if ($departmentHierarchy->getProduct() === $this) {
                $departmentHierarchy->setProduct(null);
            }
        }

        return $this;
    }

    public function getSiteCode(): ?string
    {
        return $this->siteCode;
    }

    public function setSiteCode(?string $siteCode): self
    {
        $this->siteCode = $siteCode;

        return $this;
    }

    /**
     * @return Collection|Filter[]
     */
    public function getFilters(): Collection
    {
        return $this->filters;
    }

    public function addFilter(Filter $filter): self
    {
        if (!$this->filters->contains($filter)) {
            $this->filters[] = $filter;
        }

        return $this;
    }

    public function removeFilter(Filter $filter): self
    {
        $this->filters->removeElement($filter);

        return $this;
    }

    /**
     * @return Collection|Variation[]
     */
    public function getVariations(): Collection
    {
        return $this->variations;
    }

    public function addVariation(Variation $variation): self
    {
        if (!$this->variations->contains($variation)) {
            $this->variations[] = $variation;
            $variation->setProduct($this);
        }

        return $this;
    }

    public function removeVariation(Variation $variation): self
    {
        if ($this->variations->removeElement($variation)) {
            // set the owning side to null (unless already changed)
            if ($variation->getProduct() === $this) {
                $variation->setProduct(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getVendorId(): ?int
    {
        return $this->vendorId;
    }

    public function setVendorId(?int $vendorId): self
    {
        $this->vendorId = $vendorId;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduct($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getProduct() === $this) {
                $image->setProduct(null);
            }
        }

        return $this;
    }
}
