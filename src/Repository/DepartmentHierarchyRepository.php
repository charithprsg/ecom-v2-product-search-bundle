<?php

namespace Esol\ProductSearchBundle\Repository;

use Esol\ProductSearchBundle\Entity\DepartmentHierarchy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DepartmentHierarchy|null find($id, $lockMode = null, $lockVersion = null)
 * @method DepartmentHierarchy|null findOneBy(array $criteria, array $orderBy = null)
 * @method DepartmentHierarchy[]    findAll()
 * @method DepartmentHierarchy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentHierarchyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DepartmentHierarchy::class);
    }

    // /**
    //  * @return DepartmentHierarchy[] Returns an array of DepartmentHierarchy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DepartmentHierarchy
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
