<?php

namespace Esol\ProductSearchBundle\Service;

use Esol\ProductSearchBundle\Entity\Product;
use Esol\ProductSearchBundle\Service\Application\ProductApplicationServiceInterface;

class ProductSearchManager implements ProductSearchManagerInterface
{
    /**
     * @var ProductApplicationServiceInterface
     */
    private  $productApplicationService;

    /**
     * ProductSearchManager constructor.
     */
    public function __construct(ProductApplicationServiceInterface $productApplicationService)
    {
        $this->productApplicationService = $productApplicationService;
    }

    public function getProductByErpCode(string $erpCode)
    {
        return $this->productApplicationService->getProductByErpCode($erpCode);
    }
}