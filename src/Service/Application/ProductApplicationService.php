<?php

namespace Esol\ProductSearchBundle\Service\Application;

use Esol\ProductSearchBundle\Entity\Product;
use Esol\ProductSearchBundle\Entity\Variation;
use Esol\ProductSearchBundle\Service\Domain\VariationDomainService;
use Esol\ProductSearchBundle\Service\Exception\ProductNotFoundException;

class ProductApplicationService implements ProductApplicationServiceInterface
{
    private $entityManager;

    /**
     * ProductApplicationService constructor.
     */
    public function __construct(\Doctrine\ORM\EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getProductByErpCode(string $erpCode):Product
    {
        $variation = $this->entityManager->getRepository(Variation::class)->findOneBy(array('erp' => $erpCode));
        if($variation==null){
            throw new ProductNotFoundException();
        }
        $variation = VariationDomainService::getVariation($variation);
        return $variation->getProduct();

    }
}