<?php


namespace Esol\ProductSearchBundle\Service\Domain;


use Esol\ProductSearchBundle\Entity\Variation;
use Esol\ProductSearchBundle\Service\Exception\VariationNotFoundException;

class VariationDomainService implements VariationDomainServiceInterface
{

    public static function getVariation(Variation $variation): Variation
    {
        if(!$variation->getIsActive() || $variation->getIsDeleted()){
            throw new VariationNotFoundException();
        }
        return $variation;
    }
}