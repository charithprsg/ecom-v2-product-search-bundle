<?php

namespace Esol\ProductSearchBundle\Tests\Service;

use Esol\ProductSearchBundle\Service\ProductSearchManager;
use PHPUnit\Framework\TestCase;

class ProductSearchManagerTest extends TestCase
{
    public function test(){
        $manager =  new ProductSearchManager();
        $value = $manager->productSearch();
        $this->assertEquals(10, $value);
    }
}