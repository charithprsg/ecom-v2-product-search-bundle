<?php

namespace Esol\ProductSearchBundle\Repository;

use Esol\ProductSearchBundle\Entity\FilterItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FilterItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method FilterItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method FilterItem[]    findAll()
 * @method FilterItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FilterItem::class);
    }

    // /**
    //  * @return FilterItem[] Returns an array of FilterItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FilterItem
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
